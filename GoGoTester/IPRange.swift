//
//  IPRange.swift
//  GoGoTester
//
//  Created by netokey on 25/08/2014.
//  Copyright (c) 2014 Justin Zhang. All rights reserved.
//

import Foundation

class IPRange {
    var cope : [[Int]] = [[0, 0], [0, 0], [0, 0], [0, 0]]
    
    var count : Int {
        var sum = 1
        for i in 0...3 {
            sum *= self.cope[i][1]-self.cope[i][0]
            }
        return sum
    }
    
    func GetRandomIP() ->String {
        //获取[0,X)的方法Int(arc4random_uniform(UInt32(X)))
        var sbd=String(cope[0][0]+Int(arc4random_uniform(UInt32(cope[0][1]-cope[0][0]+1))))
        for i in 1...3 {
            sbd += "." + String(cope[i][0]+Int(arc4random_uniform(UInt32(cope[i][1]-cope[i][0]+1))))
        }
        return sbd
    }
}