//
//  AppDelegate.swift
//  GoGoTester
//
//  Created by netokey on 21/08/2014.
//  Copyright (c) 2014 Justin Zhang. All rights reserved.
//

import Cocoa

class AppDelegate: NSObject, NSApplicationDelegate {
                            
    @IBOutlet weak var window: NSWindow!
    //自定义Outlet区
    @IBOutlet weak var btnRndTest: NSButton!
    @IBOutlet weak var btnStdTest: NSButton!
    @IBOutlet weak var btnProxyTest: NSButton!
    @IBOutlet weak var btnStopTest: NSButton!
    

    func applicationDidFinishLaunching(aNotification: NSNotification?) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(aNotification: NSNotification?) {
        // Insert code here to tear down your application
    }

    //自定义方法区
    @IBAction func btnPressed(sender: AnyObject) {
        let Sender = sender as NSButton
        if Sender == btnStopTest {
            btnRndTest.enabled = true
            btnStdTest.enabled = true
            btnProxyTest.enabled = true
            btnStopTest.enabled = false
        }
        else {
            btnRndTest.enabled = false
            btnStdTest.enabled = false
            btnProxyTest.enabled = false
            btnStopTest.enabled = true
        }
    }
    
}

